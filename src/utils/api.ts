import axios, { AxiosError } from 'axios'
import md5 from 'md5'

const apiPassword = process.env.API_PASSWORD || 'Valantis'

export const getApiStamp = new Date().toISOString().split('T')[0].replace(/-/g, '')

const apiUrl = process.env.API_KEY || 'https://api.valantis.store:41000/'

export const commonApiHeaders = {
  'X-Auth': md5(`${apiPassword ?? ''}_${getApiStamp}`),
  'Content-Type': 'application/json',
}

export const handleApiError = (error: unknown, message: string) => {
  if (axios.isAxiosError(error)) {
    const axiosError = error as AxiosError
    console.error(
      `${message}:`,
      axiosError.response ? axiosError.response.data : axiosError.message,
    )
  } else {
    console.error('An unexpected error occurred:', error)
  }
}

export const makeApiCall = async (action: string, params?: { [key: string]: unknown }) => {
  const MAX_RETRIES = 3

  const retryableApiCall = async () => {
    try {
      const response = await axios.post(apiUrl, { action, params }, { headers: commonApiHeaders })

      return response.data.result
    } catch (error) {
      handleApiError(error, 'API Error')
      throw error
    }
  }

  let retries = 0

  while (retries < MAX_RETRIES) {
    try {
      return await retryableApiCall()
    } catch (error) {
      console.error(`Error in API call: ${error}`)
      retries++
    }
  }
}
