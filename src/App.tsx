import ProductList from './components/ProductList'
import GlobalStyles from './components/theme/GlobalStyles'

const App = () => {
  return (
    <>
      <GlobalStyles />
      <ProductList />
    </>
  )
}

export default App
