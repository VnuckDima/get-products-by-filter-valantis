import styled from 'styled-components'

export const Wrapper = styled.div`
  margin: 10px;
`

export const TitleWrapper = styled.h2``

export const LabelSelect = styled.label`
  margin: 20px;
`

export const Select = styled.select`
  margin-left: 20px;
`

export const Option = styled.option``

export const Button = styled.button``

export const ProductsWrapper = styled.div`
  width: 100%;
  height: auto;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
`

export const PaginationWrapper = styled.div`
  width: 100%;
  height: auto;
  display: flex;
  justify-content: center;
  align-items: center;
`
