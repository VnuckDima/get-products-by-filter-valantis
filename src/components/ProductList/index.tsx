import { memo, useCallback, useEffect, useMemo, useState } from 'react'
import { handleApiError, makeApiCall } from 'src/utils/api'
import { debounce } from 'src/utils/debounce'

import Card from '../Card'
import {
  Button,
  LabelSelect,
  Option,
  PaginationWrapper,
  ProductsWrapper,
  Select,
  TitleWrapper,
  Wrapper,
} from './styled'

interface Product {
  id: string
  product: string
  price: number
  brand: string | null
}

const ProductList = () => {
  const [productIds, setProductIds] = useState<string[]>([])
  const [selectedProducts, setSelectedProducts] = useState<Product[]>([])
  const [productsOffset, setProductsOffset] = useState(0)
  const [filterField, setFilterField] = useState<string>('')
  const [filterValue, setFilterValue] = useState<string | number>('')

  const [filterOptions, setFilterOptions] = useState<string[]>([])
  const [secondSelectOptions, setSecondSelectOptions] = useState<string[]>([])

  const [loading, setLoading] = useState(true)

  const PRODUCTS_PER_PAGE = 50

  const handleFilter = useMemo(
    () =>
      debounce(async () => {
        try {
          const params = {
            [filterField]: filterField === 'price' ? +filterValue : filterValue,
          }

          const result = await makeApiCall('filter', params)
          setProductIds(result)
        } catch (error) {
          handleApiError(error, 'Error applying filter')
        }
      }, 300),
    [filterField, filterValue],
  )

  const debouncedFilter = useMemo(() => debounce(handleFilter, 500), [handleFilter])

  useEffect(() => {
    const fetchData = async () => {
      try {
        const [response, filterFieldsResponse] = await Promise.all([
          makeApiCall('get_ids', { offset: productsOffset, limit: PRODUCTS_PER_PAGE }),
          makeApiCall('get_fields'),
        ])

        setProductIds(response)

        setFilterOptions(filterFieldsResponse.filter(Boolean))
        setLoading(false)
      } catch (error: unknown) {
        handleApiError(error, 'Error fetching fields')
      }
    }

    fetchData()
  }, [productsOffset])

  const isFilterField = ['brand', 'price', 'product'].includes(filterField)

  useEffect(() => {
    if (isFilterField) {
      const fetchSecondSelectOptions = async () => {
        try {
          const response = await makeApiCall('get_fields', { field: filterField })

          setSecondSelectOptions(response.filter(Boolean))
        } catch (error: unknown) {
          handleApiError(error, 'Error fetching second select options')
        }
      }

      fetchSecondSelectOptions()
    }
  }, [filterField, filterValue, isFilterField])

  useEffect(() => {
    if (productIds.length > 0) {
      const handleGetItems = async () => {
        try {
          const response = await makeApiCall('get_items', { ids: productIds })

          const uniqueProducts = [...new Set(response.map((product: Product) => product.id))].map(
            (id) => response.find((product: Product) => product.id === id),
          )

          setSelectedProducts(uniqueProducts)
        } catch (error: unknown) {
          handleApiError(error, 'Error fetching fields')
          handleGetItems()
        }
      }

      handleGetItems()
    }
  }, [productIds])

  const handleFilterButtonClick = useCallback(() => {
    debouncedFilter()
  }, [debouncedFilter])

  const handlePagination = (offsetChange: number) => () => {
    setProductsOffset((prevOffset) => prevOffset + offsetChange)
  }

  const handlePrevPage = handlePagination(-50)
  const handleNextPage = handlePagination(50)

  const isDisabledPagination = productsOffset === 0

  if (loading) return <div>Loading...</div>

  return (
    <Wrapper>
      <TitleWrapper>Selected Products</TitleWrapper>
      <Wrapper>
        <LabelSelect>
          Filter Field:
          <Select value={filterField} onChange={(e) => setFilterField(e.target.value)}>
            <Option>Select Filter</Option>
            {filterOptions.map((option, index) => (
              <Option key={index} value={option}>
                {option}
              </Option>
            ))}
          </Select>
        </LabelSelect>

        <Button onClick={handleFilterButtonClick}>Apply Filter</Button>
      </Wrapper>

      <Wrapper>
        <LabelSelect>
          Second Select:
          <Select onChange={(e) => setFilterValue(e.target.value)}>
            <Option>Select...</Option>
            {secondSelectOptions.map((option, index) => (
              <Option key={index} value={filterField === 'price' ? +option : option}>
                {filterField === 'price' ? +option : option}
              </Option>
            ))}
          </Select>
        </LabelSelect>
      </Wrapper>
      {selectedProducts.length > 0 && (
        <PaginationWrapper>
          <Wrapper>
            <Button onClick={handlePrevPage} disabled={isDisabledPagination}>
              Prev
            </Button>
          </Wrapper>
          <Wrapper>
            <Button onClick={handleNextPage}>Next</Button>
          </Wrapper>
        </PaginationWrapper>
      )}
      <Wrapper>
        <TitleWrapper>Products</TitleWrapper>
        <ProductsWrapper>
          {selectedProducts.map((product: Product) => (
            <Card key={product.id} {...product} />
          ))}
        </ProductsWrapper>
      </Wrapper>
    </Wrapper>
  )
}

export default memo(ProductList)
