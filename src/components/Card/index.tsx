import { FC } from 'react'

import { CardWrapper, InfoProduct } from './styled'

interface CardProps {
  id: string
  product: string
  price: number
  brand: string | null
}

const Card: FC<CardProps> = ({ id, product, price, brand }) => {
  return (
    <CardWrapper>
      <InfoProduct>{id}</InfoProduct>
      <InfoProduct>{product}</InfoProduct>
      <InfoProduct>{price}</InfoProduct>
      <InfoProduct>{brand}</InfoProduct>
    </CardWrapper>
  )
}

export default Card
