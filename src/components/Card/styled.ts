import styled from 'styled-components'

export const CardWrapper = styled.div`
  width: 250px;
  height: 300px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  margin: 10px;
  border: 1px solid gray;
`

export const InfoProduct = styled.div`
  text-align: center;
`
